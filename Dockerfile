FROM node:18-alpine3.14 as builder

WORKDIR /home/node/app

COPY package.json package-lock.json ./

RUN npm ci --omit=dev

COPY . .

#################################

FROM node:18-alpine3.14

WORKDIR /home/node/app

USER node

COPY --from=builder /home/node/app .

EXPOSE 3000

CMD [ "npm", "start" ]
