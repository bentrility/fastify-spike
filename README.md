# fastify spike

The purpose of this project is to show an example of how we could structure our api.

### Problems Solved

 - Verify (one command to know it's safe to check in) - done
 - Where post deployment checks (smoke tests) go - done
 - Route registration - done
 - Request validation - done
 - JWT validation - done
 - Persistence / sharing of db migrations in mysql - tbd
