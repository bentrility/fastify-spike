import frisby from 'frisby';

import {createTwoLeggedJwt, getBaseUrl} from '../../../utils.js';

describe('GET /cats/:id', () => {
    test('should return a cat by id', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/1/authenticated`, {
            headers: {
                authorization: `Bearer ${createTwoLeggedJwt()}`,
            },
        });

        expect(response.status).toBe(200);
        expect(response.json).toStrictEqual(
            {
                id: 1,
                name: 'Fitz',
            },
        );
    });

    test('should return not found when a cat does not exist', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`, {
            headers: {
                authorization: `Bearer ${createTwoLeggedJwt()}`,
            },
        });

        expect(response.status).toBe(404);
        expect(response.json).toStrictEqual(
            {
                error: 'Not Found',
                message: 'Not Found',
                statusCode: 404,
            },
        );
    });

    test('should return 401 when missing auth header', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`);

        expect(response.status).toBe(401);
        expect(response.json).toStrictEqual(
            {
                error: 'Unauthorized',
                message: 'Unauthorized',
                statusCode: 401,
            },
        );
    });

    test('should return 401 when malformed jwt', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`, {
            headers: {
                authorization: 'Bearer fff',
            },
        });

        expect(response.status).toBe(401);
        expect(response.json).toStrictEqual(
            {
                error: 'Unauthorized',
                message: 'jwt malformed',
                statusCode: 401,
            },
        );
    });

    test('should return 401 when missing jwt', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`, {
            headers: {
                authorization: 'Bearer ',
            },
        });

        expect(response.status).toBe(401);
        expect(response.json).toStrictEqual(
            {
                error: 'Unauthorized',
                message: 'jwt must be provided',
                statusCode: 401,
            },
        );
    });

    test('should return 401 when invalid auth header', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`, {
            headers: {
                authorization: 'fff',
            },
        });

        expect(response.status).toBe(401);
        expect(response.json).toStrictEqual(
            {
                error: 'Unauthorized',
                message: 'jwt must be provided',
                statusCode: 401,
            },
        );
    });

    test('should return 401 when unauthorized client', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50/authenticated`, {
            headers: {
                authorization: `Bearer ${createTwoLeggedJwt(chance.string())}`,
            },
        });

        expect(response.status).toBe(401);
        expect(response.json).toStrictEqual(
            {
                error: 'Unauthorized',
                message: 'Unauthorized',
                statusCode: 401,
            },
        );
    });
});
