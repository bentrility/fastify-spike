import frisby from 'frisby';

import {getBaseUrl} from '../../utils.js';

describe('GET /cats/:id', () => {
    test('should return a cat by id', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/1`);

        expect(response.status).toBe(200);
        expect(response.json).toStrictEqual(
            {
                id: 1,
                name: 'Fitz',
            },
        );
    });

    test('should return not found when a cat does not exist', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats/50`);

        expect(response.status).toBe(404);
        expect(response.json).toStrictEqual(
            {
                error: 'Not Found',
                message: 'Not Found',
                statusCode: 404,
            },
        );
    });
});
