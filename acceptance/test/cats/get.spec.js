import frisby from 'frisby';

import {getBaseUrl} from '../utils.js';

describe('GET /cats', () => {
    test('should return all cats', async () => {
        const response = await frisby.get(`${getBaseUrl()}/cats`);

        expect(response.status).toBe(200);
        expect(response.json).toStrictEqual([
            {
                id: 1,
                name: 'Fitz',
            },
            {
                id: 2,
                name: 'Lola',
            },
        ]);
    });
});
