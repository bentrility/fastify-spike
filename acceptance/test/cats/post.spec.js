import frisby from 'frisby';

import {getBaseUrl} from '../utils.js';

describe('POST /cats', () => {
    test('should support creating a cat', async () => {
        const body = {
            name: chance.string(),
        };

        const response = await frisby.post(`${getBaseUrl()}/cats`, {
            body,
        });

        expect(response.status).toBe(201);
        expect(response.json).toStrictEqual({
            ...body,
            id: expect.any(Number),
        });

        const deleteResponse = await frisby.del(`${getBaseUrl()}/cats/${response.json.id}`);

        expect(deleteResponse.status).toBe(204);

        const allCatsResponse = await frisby.get(`${getBaseUrl()}/cats`);

        expect(allCatsResponse.status).toBe(200);
        expect(allCatsResponse.json).toHaveLength(2);
    });

    test('should validate the input', async () => {
        const response = await frisby.post(`${getBaseUrl()}/cats`, {
            body: {},
        });

        expect(response.status).toBe(400);
        expect(response.json).toStrictEqual({
            error: 'Bad Request',
            message: 'body must have required property \'name\'',
            statusCode: 400,
        });
    });
});
