import frisby from 'frisby';

import {getBaseUrl} from './utils.js';

const correlationIdHeader = 'x-anxrs-correlation-id';

describe('request id', () => {
    test('should return the correlation id', async () => {
        const response = await frisby.get(`${getBaseUrl()}/healthz`);

        expect(response.status).toBe(200);

        expect(response.headers.get(correlationIdHeader)).toStrictEqual(expect.any(String));
    });

    test('should use the correlation id that was passed', async () => {
        const correlationId = chance.guid();

        const response = await frisby.get(`${getBaseUrl()}/healthz`, {
            headers: {
                [correlationIdHeader]: correlationId,
            },
        });

        expect(response.status).toBe(200);

        expect(response.headers.get(correlationIdHeader)).toBe(correlationId);
    });
});
