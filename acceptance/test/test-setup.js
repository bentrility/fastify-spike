import Chance from 'chance';
import frisby from 'frisby';

global.chance = new Chance();

frisby.globalSetup({
    timeout: 10_000,
});
