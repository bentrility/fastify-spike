import jwt from 'jsonwebtoken';

const getBaseUrl = () => 'http://127.0.0.1:3000';

const createTwoLeggedJwt = (clientId = 'fakeTrustedClient') => jwt.sign({
    clientId,
}, 'keep_it_secret');

export {
    createTwoLeggedJwt,
    getBaseUrl,
};
