/* eslint-disable unicorn/no-process-exit, no-console, no-await-in-loop */
import process from 'node:process';

import frisby from 'frisby';

frisby.globalSetup({
    timeout: 10_000,
});

const maxAttempts = 10;
const sleepPeriod = 500;
const allServices = [
    {
        name: 'fastify-spike',
        url: 'http://localhost:3000/healthz',
    },
];

// eslint-disable-next-line no-promise-executor-return
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

// eslint-disable-next-line consistent-return
const healthcheck = async (services, attempt = 0) => {
    if (attempt >= maxAttempts) {
        console.log('Services failed to become healthy.');

        process.exit(1);
    }

    for (const service of services) {
        if (!service.healthy) {
            try {
                const response = await frisby.get(service.url);

                if (response.status === 200) {
                    service.healthy = true;
                }
            } catch {}
        }
    }

    if (services.some((it) => !it.healthy)) {
        process.stdout.write('.');
        await sleep(sleepPeriod);

        return healthcheck(services, attempt + 1);
    }

    console.log('All services healthy. It\'s go time.');
    process.exit(0);
};

healthcheck(allServices);

/* eslint-enable unicorn/no-process-exit, no-console, no-await-in-loop */
