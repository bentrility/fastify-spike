import frisby from 'frisby';

import {getBaseUrl} from '../utils.js';

describe('smoke test', () => {
    test('should be an example smoke test', async () => {
        const response = await frisby.get(`${getBaseUrl()}/healthz`);

        expect(response.status).toBe(200);
    });
});
