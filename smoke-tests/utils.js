import process from 'node:process';

export const getBaseUrl = () => process.env.BASE_URL || 'http://127.0.0.1:3000';
