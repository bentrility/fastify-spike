import {fileURLToPath} from 'node:url';
import {dirname, join} from 'node:path';

import {v4} from 'uuid';
import config from 'config';
import fastify from 'fastify';
import autoload from '@fastify/autoload';
import auth from '@fastify/auth';

import {jwtValidation} from './infra/jwt-validation.js';

/*
    Most of this feels like boilerplate that could be abstracted to a start app function as we add more services.
 */
const correlationIdHeader = 'x-anxrs-correlation-id';

const app = fastify({
    genReqId: v4,
    logger: true,
    requestIdHeader: correlationIdHeader,
    requestIdLogLabel: 'correlationId',
});

app.decorate('verifyJwt', jwtValidation);

app.addHook('preHandler', (req, reply, done) => {
    reply.header(correlationIdHeader, req.id);

    done();
});

app.register(auth);

app.register(autoload, {
    dir: join(dirname(fileURLToPath(import.meta.url)), 'routes'),
});

app.listen(config.get('port'), config.get('host'));
