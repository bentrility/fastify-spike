import createError from 'http-errors';
import jwt from 'jsonwebtoken';

export const jwtValidation = (request, reply, done) => {
    const authHeader = request.headers.authorization;

    if (!authHeader) {
        throw createError.Unauthorized();
    }

    const [, token] = authHeader.split(' ');

    // this is example code to show what registering an auth strategy is like
    const claims = jwt.verify(token, 'keep_it_secret');

    if (claims.clientId !== 'fakeTrustedClient') {
        throw createError.Unauthorized();
    }

    done();
};
