import createError from 'http-errors';

let allCats = [
    {
        id: 1,
        name: 'Fitz',
    },
    {
        id: 2,
        name: 'Lola',
    },
];

const create = (catModel) => {
    const nextId = allCats.length + 1;

    const created = {
        ...catModel,
        id: nextId,
    };

    allCats.push(created);

    return created;
};

const getAll = () => allCats;

const getById = (id) => allCats.find((it) => it.id.toString() === id.toString());

const deleteCat = (id) => {
    const found = getById(id);

    if (!found) {
        throw new createError.NotFound();
    }

    allCats = allCats.filter((cat) => String(cat.id) !== String(id));
};

export {
    create,
    deleteCat,
    getAll,
    getById,
};
