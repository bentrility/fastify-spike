import createError from 'http-errors';

import {getAll, getById, create, deleteCat} from '../../repositories/cats.js';

/*
    I would like to get this to the point that have each of these in a separate file,
    ideally in a unique folder based on the route you want to use.
 */

const getCatById = (request) => {
    const found = getById(request.params.id);

    if (found) {
        return found;
    }

    throw new createError.NotFound();
};

const deleteById = (request, reply) => {
    deleteCat(request.params.id);

    return reply.code(204).send();
};

const createCat = ({body}, reply) => {
    const created = create(body);

    return reply.code(201).send(created);
};

export default async (app) => {
    await app.get('/', () => getAll());
    await app.post('/', {
        handler: createCat,
        schema: {
            body: {
                properties: {
                    name: {type: 'string'},
                },
                required: ['name'],
                type: 'object',
            },
        },
    });

    await app.get('/:id', getCatById);
    await app.delete('/:id', deleteById);

    await app.route({
        handler: getCatById,
        method: 'GET',
        preHandler: app.auth([app.verifyJwt]),
        url: '/:id/authenticated',
    });
};
