export default async (fastify) => {
    await fastify.get('/', () => ({
        status: 'healthy',
    }));
};
