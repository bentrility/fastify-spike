import {fileURLToPath} from 'node:url';
import {dirname, join} from 'node:path';

import {v4} from 'uuid';
import fastify from 'fastify';
import config from 'config';
import autoload from '@fastify/autoload';
import auth from '@fastify/auth';

import {jwtValidation} from '../src/infra/jwt-validation.js';

jest.mock('fastify');
jest.mock('@fastify/autoload');
jest.mock('@fastify/auth');
jest.mock('config');
jest.mock('node:url');
jest.mock('node:path');
jest.mock('uuid');
jest.mock('../src/infra/jwt-validation.js');

describe('app', () => {
    let app,
        host,
        fileName,
        directory,
        path,
        port;

    beforeEach(() => {
        app = {
            addHook: jest.fn(),
            decorate: jest.fn(),
            listen: jest.fn(),
            register: jest.fn(),
        };
        port = chance.natural();
        host = chance.string();
        fileName = chance.string();
        directory = chance.string();
        path = chance.string();

        fileURLToPath.mockImplementation(() => fileName);
        dirname.mockImplementation(() => directory);
        join.mockImplementation(() => path);
        config.get.mockImplementation((key) => {
            const mockedConfig = {
                host,
                port,
            };

            return mockedConfig[key];
        });
        fastify.mockReturnValue(app);

        jest.isolateModules(() => {
            require('../src/index.js');
        });
    });

    test('should call autoload', () => {
        expect(fileURLToPath).toHaveBeenCalledTimes(1);
        expect(fileURLToPath).toHaveBeenCalledWith(expect.any(String)); // integration test is what matters

        expect(dirname).toHaveBeenCalledTimes(1);
        expect(dirname).toHaveBeenCalledWith(fileName);

        expect(join).toHaveBeenCalledTimes(1);
        expect(join).toHaveBeenCalledWith(directory, 'routes');

        expect(app.register).toHaveBeenCalledTimes(2);
        expect(app.register).toHaveBeenCalledWith(autoload, {
            dir: path,
        });
        expect(app.register).toHaveBeenCalledWith(auth);
    });

    test('should decorate with auth', () => {
        expect(app.decorate).toHaveBeenCalledTimes(1);
        expect(app.decorate).toHaveBeenCalledWith('verifyJwt', jwtValidation);
    });

    test('should create an app', () => {
        expect(fastify).toHaveBeenCalledTimes(1);
        expect(fastify).toHaveBeenCalledWith({
            genReqId: v4,
            logger: true,
            requestIdHeader: 'x-anxrs-correlation-id',
            requestIdLogLabel: 'correlationId',
        });
    });

    test('should add the correlation id to all responses', () => {
        expect(app.addHook).toHaveBeenCalledTimes(1);
        expect(app.addHook).toHaveBeenCalledWith('preHandler', expect.any(Function));

        const preHandlerHook = app.addHook.mock.calls[0][1];

        const done = jest.fn();
        const request = {
            id: chance.guid(),
        };
        const reply = {
            header: jest.fn(),
        };

        preHandlerHook(request, reply, done);

        expect(reply.header).toHaveBeenCalledTimes(1);
        expect(reply.header).toHaveBeenCalledWith('x-anxrs-correlation-id', request.id);
        expect(done).toHaveBeenCalledTimes(1);
    });

    test('should call listen', () => {
        expect(config.get).toHaveBeenCalledTimes(2);
        expect(config.get).toHaveBeenCalledWith('host');
        expect(config.get).toHaveBeenCalledWith('port');

        expect(app.listen).toHaveBeenCalledTimes(1);
        expect(app.listen).toHaveBeenCalledWith(port, host);
    });
});
