import jwt from 'jsonwebtoken';

import {jwtValidation} from '../../src/infra/jwt-validation.js';

jest.mock('jsonwebtoken');

describe('jwtValidation', () => {
    let request,
        done,
        claims,
        token;

    beforeEach(() => {
        done = jest.fn();
        token = chance.string();
        request = {
            headers: {
                authorization: `Bearer ${token}`,
            },
        };

        claims = {
            clientId: 'fakeTrustedClient',
        };

        jwt.verify.mockReturnValue(claims);
    });

    test('should reject missing auth header', () => {
        delete request.headers.authorization;

        expect(() => jwtValidation(request, undefined, done)).toThrow('Unauthorized');
    });

    test('should reject an invalid client id', () => {
        claims.clientId = chance.string();

        expect(() => jwtValidation(request, undefined, done)).toThrow('Unauthorized');
    });

    test('should verify the token', () => {
        jwtValidation(request, undefined, done);

        expect(jwt.verify).toHaveBeenCalledTimes(1);
        expect(jwt.verify).toHaveBeenCalledWith(token, 'keep_it_secret');

        expect(done).toHaveBeenCalledTimes(1);
    });
});
