describe('cats repository', () => {
    let catRepository;

    beforeEach(() => {
        jest.isolateModules(() => {
            catRepository = require('../../src/repositories/cats.js');
        });
    });

    describe('getAll', () => {
        test('should return all', () => {
            const results = catRepository.getAll();

            expect(results).toStrictEqual([
                {
                    id: 1,
                    name: 'Fitz',
                },
                {
                    id: 2,
                    name: 'Lola',
                },
            ]);
        });
    });

    describe('getById', () => {
        test('should return found value', () => {
            const all = catRepository.getAll();
            const random = chance.pickone(all);

            const result = catRepository.getById(random.id);

            expect(result).toBe(random);
        });

        test('should return found value when it is a string', () => {
            const all = catRepository.getAll();
            const random = chance.pickone(all);

            const result = catRepository.getById(String(random.id));

            expect(result).toBe(random);
        });

        test('should return undefined when it does not exist', () => {
            const result = catRepository.getById(chance.integer({min: 3}));

            expect(result).toBeUndefined();
        });
    });

    describe('create', () => {
        test('should create a cat', () => {
            const model = {
                [chance.string()]: chance.string(),
            };

            const originalLength = catRepository.getAll().length;

            const result = catRepository.create(model);

            expect(result).toStrictEqual({
                ...model,
                id: originalLength + 1,
            });
        });
    });

    describe('delete', () => {
        test('should delete a cat and match regardless of type', () => {
            expect(catRepository.getAll()).toHaveLength(2);

            const result = catRepository.deleteCat('1');

            expect(result).toBeUndefined();

            expect(catRepository.getAll()).toHaveLength(1);
        });

        test('should fail delete when the cat is not found', () => {
            expect(() => catRepository.deleteCat(chance.natural({min: 2}))).toThrow('Not Found');
        });
    });
});
