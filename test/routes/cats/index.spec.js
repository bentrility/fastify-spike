import register from '../../../src/routes/cats/index.js';
import {getAll, getById, create, deleteCat} from '../../../src/repositories/cats.js';

jest.mock('../../../src/repositories/cats.js');

describe('cat routes', () => {
    let app,
        authResponse;

    beforeEach(async () => {
        app = {
            auth: jest.fn(),
            delete: jest.fn(),
            get: jest.fn(),
            post: jest.fn(),
            route: jest.fn(),
            verifyJwt: jest.fn(),
        };

        authResponse = chance.string();

        app.auth.mockReturnValue(authResponse);
        await register(app);
    });

    test('should register routes', () => {
        expect(app.get).toHaveBeenCalledTimes(2);
        expect(app.get).toHaveBeenCalledWith('/', expect.any(Function));
        expect(app.get).toHaveBeenCalledWith('/:id', expect.any(Function));

        expect(app.post).toHaveBeenCalledTimes(1);
        expect(app.post).toHaveBeenCalledWith('/', {
            handler: expect.any(Function),
            schema: {
                body: {
                    properties: {
                        name: {type: 'string'},
                    },
                    required: ['name'],
                    type: 'object',
                },
            },
        });

        expect(app.delete).toHaveBeenCalledTimes(1);
        expect(app.delete).toHaveBeenCalledWith('/:id', expect.any(Function));

        expect(app.route).toHaveBeenCalledTimes(1);
        expect(app.route).toHaveBeenCalledWith({
            handler: expect.any(Function),
            method: 'GET',
            preHandler: authResponse,
            url: '/:id/authenticated',
        });

        expect(app.auth).toHaveBeenCalledTimes(1);
        expect(app.auth).toHaveBeenCalledWith([app.verifyJwt]);
    });

    describe('GET /cats', () => {
        let handler,
            cats;

        beforeEach(() => {
            cats = chance.n(() => ({
                name: chance.first(),
            }), chance.d4());

            handler = app.get.mock.calls[0][1];

            getAll.mockReturnValue(cats);
        });

        test('should register /cats route', () => {
            const response = handler();

            expect(getAll).toHaveBeenCalledTimes(1);
            expect(response).toStrictEqual(cats);
        });
    });

    describe('POST /cats', () => {
        let handler,
            created,
            reply,
            request,
            sendResponse;

        beforeEach(() => {
            request = {
                body: {
                    [chance.string()]: chance.string(),
                },
            };

            created = {
                [chance.string()]: chance.string(),
            };

            create.mockReturnValue(created);
            sendResponse = chance.string();

            reply = {
                code: jest.fn().mockReturnValue({
                    send: jest.fn().mockReturnValue(sendResponse),
                }),
            };

            handler = app.post.mock.calls[0][1].handler;
        });

        test('should set up create /cats route', () => {
            const response = handler(request, reply);

            expect(response).toStrictEqual(sendResponse);

            expect(create).toHaveBeenCalledTimes(1);
            expect(create).toHaveBeenCalledWith(request.body);

            expect(reply.code).toHaveBeenCalledTimes(1);
            expect(reply.code).toHaveBeenCalledWith(201);
        });
    });

    describe('DELETE /cats/:id', () => {
        let catId,
            handler,
            reply,
            request,
            sendResponse;

        beforeEach(() => {
            catId = chance.string();

            request = {
                params: {
                    id: catId,
                },
            };

            sendResponse = chance.string();

            reply = {
                code: jest.fn().mockReturnValue({
                    send: jest.fn().mockReturnValue(sendResponse),
                }),
            };

            handler = app.delete.mock.calls[0][1];
        });

        test('should set up delete /cats/:id route', () => {
            const response = handler(request, reply);

            expect(response).toStrictEqual(sendResponse);

            expect(deleteCat).toHaveBeenCalledTimes(1);
            expect(deleteCat).toHaveBeenCalledWith(catId);

            expect(reply.code).toHaveBeenCalledTimes(1);
            expect(reply.code).toHaveBeenCalledWith(204);
        });
    });

    describe('GET /cats/:id', () => {
        let handler,
            catId,
            cat;

        beforeEach(() => {
            catId = chance.natural();
            cat = {
                name: chance.first(),
            };

            handler = app.get.mock.calls[1][1];
        });

        test('should look up cat by id and return it', () => {
            getById.mockReturnValue(cat);

            const response = handler({
                params: {
                    id: catId,
                },
            });

            expect(getById).toHaveBeenCalledTimes(1);
            expect(getById).toHaveBeenCalledWith(catId);

            expect(response).toStrictEqual(cat);
        });

        test('should throw error when cat not found', () => {
            expect(() => handler({
                params: {
                    id: catId,
                },
            })).toThrow('Not Found');
        });
    });

    describe('GET /cats/:id/authenticate', () => {
        let handler,
            catId,
            cat;

        beforeEach(() => {
            catId = chance.natural();
            cat = {
                name: chance.first(),
            };

            handler = app.route.mock.calls[0][0].handler;
        });

        test('should look up cat by id and return it', () => {
            getById.mockReturnValue(cat);

            const response = handler({
                params: {
                    id: catId,
                },
            });

            expect(getById).toHaveBeenCalledTimes(1);
            expect(getById).toHaveBeenCalledWith(catId);

            expect(response).toStrictEqual(cat);
        });

        test('should throw error when cat not found', () => {
            expect(() => handler({
                params: {
                    id: catId,
                },
            })).toThrow('Not Found');
        });
    });
});
