import register from '../../../src/routes/healthz/index.js';

describe('healthz route', () => {
    let app;

    beforeEach(async () => {
        app = {
            get: jest.fn(),
        };

        await register(app);
    });

    test('should register route', () => {
        expect(app.get).toHaveBeenCalledTimes(1);
        expect(app.get).toHaveBeenCalledWith('/', expect.any(Function));
    });

    test('should return healthy response', () => {
        const handler = app.get.mock.calls[0][1];

        const response = handler();

        expect(response).toStrictEqual({
            status: 'healthy',
        });
    });
});
